# Gradients

    for (i=0; i<n; ++i)
    {
      c[i].R=start.R+(i*(end.R-start.R))/n;
      c[i].G=start.G+(i*(end.G-start.G))/n;
      c[i].B=start.B+(i*(end.B-start.B))/n;
    }

### Output
+ Text
+ PNG
+ PPM 
+ XML
+ Json
+ slk
+ Excel (*background*)

### Python
### C++

## Classes

### Color
+ R
+ G
+ B
+ virtual Output() 

### Point
+ Position
+ Color

### Gradient
+ Size
+ Name
+ Colors[]
+ Map[]
+ Load(filename)
+ Save(filename)
+ AddPoint()
+ SortPoints()
+ GenerateMap()
+ SaveMap(filename)
+ MapToPPM(filename)
+ virtual Output()
