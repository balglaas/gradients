function Color(r,g,b)
{
  this.R=r;
  this.G=g;
  this.B=b
}

Color.prototype.ToHex=function()
{
  var r=this.R.toString(16);
  if (r.length==1)
  {
    r="0"+r;
  }
  var g=this.G.toString(16);
  if (g.length==1)
  {
    g="0"+g;
  }
  var b=this.B.toString(16);
  if (b.length==1)
  {
    b="0"+b;
  }
  return("#"+r+g+b);
}

Color.prototype.RGB=function()
{
  return("rgb("+this.R.toString()+", "+this.G.toString()+", "+this.B.toString()+")");	
}
