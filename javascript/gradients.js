function hexToR(h) {return parseInt((cutHex(h)).substring(0,2),16)}
function hexToG(h) {return parseInt((cutHex(h)).substring(2,4),16)}
function hexToB(h) {return parseInt((cutHex(h)).substring(4,6),16)}
function cutHex(h) {return (h.charAt(0)=="#") ? h.substring(1,7):h}

var N=256;

function CreateGradient(form)
{
  hex=form.color_start.value;	
  var color_start=new Color(hexToR(hex),hexToG(hex),hexToB(hex));
  hex=form.color_end.value;	
  var color_end=new Color(hexToR(hex),hexToG(hex),hexToB(hex));
  
  //console.log(color_end);
  var interval_r=color_end.R-color_start.R;
  var interval_g=color_end.G-color_start.G;
  var interval_b=color_end.B-color_start.B;
  start_r=color_start.R;
  start_g=color_start.G;
  start_b=color_start.B;
  //console.log(interval_r.toString()+" "+interval_g.toString()+" "+interval_b.toString());
  
  gradient[0]=color_start;
  for (var i=1; i<(N-1); ++i)
  {
    //gradient[i].r=start.r+(i*interval_r)/(N-1);
    red=start_r+Math.floor((i*interval_r)/(N-1));
    green=start_g+Math.floor((i*interval_g)/(N-1));
    blue=start_b+Math.floor((i*interval_b)/(N-1));
    //console.log(i.toString()+" "+red.toString()+" "+green.toString()+" "+blue.toString());
    //var color="rgb("+red.toString()+", "+green.toString()+", "+blue.toString()+")";
    color=new Color(red,green,blue);
    gradient[i]=color;
  }
  gradient[N-1]=color_end;
  //console.log(gradient);
}

function LoadJson(form)
{
}
function SaveJson(form)
{
  if (gradient!=undefined)
  {
    var jsondata=gradient.SaveToJson();
    filename="gradient."+Number(Date.now()).toString(16)+".json";
    console.log(filename);
    link=document.createElement('a');
    link.setAttribute("download",filename);
    link.setAttribute("href", "data:application/json,"+jsondata);
    document.body.appendChild(link); // Required for FF
    link.click();
  }
}

function ExportMap(form)
{
  CreateGradient(form);
  data="";
  for (var i=0; i<N; ++i)
  {
    color=gradient[i];
    data+=color.R+" "+color.G+" "+color.B+"%0D%0A\n";
  }
  link=document.createElement('a');
  link.setAttribute("download", "gradient.map");
  link.setAttribute("href", "data:text/plain,"+data);
  document.body.appendChild(link); // Required for FF
  link.click();
}

function ShowGradient(form)
{
  if (gradient!=undefined)
  {
    //CreateGradient(form);
    var hex=form.color_start.value;	
    var color=new Color(hexToR(hex),hexToG(hex),hexToB(hex));
    gradient.AddPoint(new Point(0,color));
    hex=form.color_end.value;	
    color=new Color(hexToR(hex),hexToG(hex),hexToB(hex));
    gradient.AddPoint(new Point(255,color));
    gradient.GenerateMap();
    var canvas = document.getElementById("Canvas");
    var context = canvas.getContext("2d");
    context.clearRect(0, 0, canvas.width, canvas.height);
    for (var i=0; i<gradient.Size; ++i)
    {
      var color=gradient.Map[i];
      context.strokeStyle=color.RGB();
      context.beginPath(); 
      context.moveTo(i,0);
      context.lineTo(i,10);
      context.stroke();
    }
  }
}

var gradient=undefined;
gradient=new Gradient(256);
