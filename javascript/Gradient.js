function Gradient(size)
{
  this.Name=undefined;
  this.Size=size;
  this.Points=[];
  this.Map=undefined;
}

Gradient.prototype.AddPoint=function(point)
{
  this.Points.push(point);
}

/* Zou static moeten zijn, maar misschien gewoon geen class-method maken. */
Gradient.prototype.LoadFromJson=function()
{
}

Gradient.prototype.SaveToJson=function()
{
  this.Map=undefined;
  return(JSON.stringify(this));
}

Gradient.prototype.GenerateMap=function()
{
  console.log(this);
  this.Map=[];
  //this.SortPoints();
  var start=this.Points[0];
  //console.log(start);
  this.Map.push(start.Color);
  for (var i=1; i<this.Points.length; ++i)
  {
    //console.log(i,this.Points[i]);
    var point=this.Points[i];
    n=point.Position-start.Position;
    var interval_r=point.Color.R-start.Color.R;
    var interval_g=point.Color.G-start.Color.G;
    var interval_b=point.Color.B-start.Color.B;
    for (var j=1; j<n; ++j)
    {
      this.Map.push(new Color(Math.floor(0.5+start.Color.R+(j*interval_r)/n),
                              Math.floor(0.5+start.Color.G+(j*interval_g)/n),
			      Math.floor(0.5+start.Color.B+(j*interval_b)/n)));
    }
    this.Map.push(point.Color)
    start=point
  }
  console.log(this.Map);
}

Gradient.prototype.SaveMap=function(filename)
{
}

Gradient.prototype.SaveMapToPPM=function(filename)
{
}

