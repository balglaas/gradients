#include <stdio.h>
#include "color.h"
#include "test.h"

char *testid="1";

struct st_color get_start()
{
  struct st_color start;

  start.r=0;
  start.g=255;
  start.b=0;
  return(start);
}

struct st_color get_end()
{
  struct st_color end;

  end.r=255;
  end.g=0;
  end.b=255;
  return(end);
}
