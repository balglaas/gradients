#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "color.h"
#include "test.h"

int initialized=0;
char *testid="2";

struct st_color get_random_color()
{
  time_t tyd;
  struct st_color color;

  if (!initialized)
  {  
    time(&tyd);
    srand(tyd);
    initialized=1;
  }
  color.r=rand()&0xFF;
  color.g=rand()&0xFF;
  color.b=rand()&0xFF;
  return(color);
}

struct st_color get_start()
{
  return(get_random_color());
}

struct st_color get_end()
{
  return(get_random_color());
}
