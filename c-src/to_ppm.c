#include <stdio.h>
#include "color.h"
#include "gradient.h"

#define HEIGHT 10

void to_ppm(char *filename)
{
  int i,j;
  FILE *fp;
  unsigned char line[768];

  fp=fopen(filename,"w");
  if (fp!=NULL)
  {
    for (i=0; i<N; ++i)
    {
      line[3*i]=gradient[i].r;
      line[3*i+1]=gradient[i].g;
      line[3*i+2]=gradient[i].b;
    }
    fputs("P3\n",fp);
    fprintf(fp,"# %s\n",filename);
    fprintf(fp,"%d %d\n",N,HEIGHT);
    fprintf(fp,"%d\n",N-1);
    for (i=0; i<HEIGHT; ++i)
    {
      for (j=0; j<3*N; ++j)
      {
        fprintf(fp,"%4d",line[j]);
	if (j && (j%17)==16)
	{
	  fputc('\n',fp);
	}
      }
      fputc('\n',fp);
    }
    fclose(fp);
  }
  else
  {
    fprintf(stderr,"to_ppm: Can't open %s",filename);
  }
}
