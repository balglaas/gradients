#include <stdio.h>
#include "color.h"

#define N 256

struct st_color gradient[N];
 
create(struct st_color start,struct st_color end)
{
  int i,interval_r,interval_g,interval_b;

  interval_r=end.r-start.r;
  interval_g=end.g-start.g;
  interval_b=end.b-start.b;
  gradient[0]=start;
  for (i=1; i<(N-1); ++i)
  {
    gradient[i].r=start.r+(i*interval_r)/(N-1);
    gradient[i].g=start.g+(i*interval_g)/(N-1);
    gradient[i].b=start.b+(i*interval_b)/(N-1);
  }
  gradient[N-1]=end;
}
