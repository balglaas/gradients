CFLAGS=-g -c 
LFLAGS=

test_2:	test_2.o test_core.o gradient.o to_map.o to_ppm.o to_slk.o
	gcc test_2.o $(LFLAGS) -o test_2 test_core.o gradient.o to_map.o to_ppm.o to_slk.o
gradient.o:	gradient.c
	gcc $(CFLAGS) gradient.c
to_map.o:	to_map.c
	gcc $(CFLAGS) to_map.c
to_ppm.o:	to_ppm.c
	gcc $(CFLAGS) to_ppm.c
to_slk.o:	to_slk.c
	gcc $(CFLAGS) to_slk.c
test_core.o:	test_core.c
	gcc $(CFLAGS) test_core.c
test_2.o:	test_2.c
	gcc $(CFLAGS) test_2.c
