#include <stdio.h>
#include <string.h>
#include "color.h"
#include "test.h"
#include "gradient.h"

extern char *testid;

char *create_filename(char *filename,char *ext)
{
  strcpy(filename,"test_");
  strcat(filename,testid);
  strcat(filename,".");
  strcat(filename,ext);
  return(filename);
}

main(int argc,char *argv[])
{
  char filename[256];
  struct st_color start,end;

  start=get_start();
  end=get_end();
  create(start,end);
  to_map(create_filename(filename,"map"));
  to_slk(create_filename(filename,"slk"));
  to_ppm(create_filename(filename,"ppm"));
}
