#include <stdio.h>
#include "color.h"
#include "gradient.h"

void to_slk(char *filename)
{
  int i;
  FILE *fp;

  fp=fopen(filename,"w");
  if (fp!=NULL)
  {
    fputs("ID;PSCALC3\n",fp);
    for (i=0; i<N; ++i)
    {
      fprintf(fp,"C;X1;Y%d;K\"%d\"\n",i+1,i);
      fprintf(fp,"C;X2;Y%d;K\"%d\"\n",i+1,gradient[i].r);
      fprintf(fp,"C;X3;Y%d;K\"%d\"\n",i+1,gradient[i].g);
      fprintf(fp,"C;X4;Y%d;K\"%d\"\n",i+1,gradient[i].b);
    }
    fputs("E\n",fp);
    fclose(fp);
  }
  else
  {
    fprintf(stderr,"to_slk: Can't open %s",filename);
  }
}
