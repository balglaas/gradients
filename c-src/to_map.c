#include <stdio.h>
#include "color.h"
#include "gradient.h"

#define HEIGTH 10

void to_map(char *filename)
{
  int i;
  FILE *fp;

  fp=fopen(filename,"w");
  if (fp!=NULL)
  {
    for (i=0; i<N; ++i)
    {
      fprintf(fp,"%d %d %d\n",gradient[i].r,gradient[i].g,gradient[i].b);
    }
    fclose(fp);
  }
  else
  {
    fprintf(stderr,"to_map: Can't open %s",filename);
  }
}
