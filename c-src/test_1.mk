CFLAGS=-g -c 
LFLAGS=

test_1:	test_1.o test_core.o gradient.o to_map.o to_ppm.o to_slk.o
	gcc test_1.o $(LFLAGS) -o test_1 test_core.o gradient.o to_map.o to_ppm.o to_slk.o
gradient.o:	gradient.c
	gcc $(CFLAGS) gradient.c
to_map.o:	to_map.c
	gcc $(CFLAGS) to_map.c
to_ppm.o:	to_ppm.c
	gcc $(CFLAGS) to_ppm.c
to_slk.o:	to_slk.c
	gcc $(CFLAGS) to_slk.c
test_core.o:	test_core.c
	gcc $(CFLAGS) test_core.c
test_1.o:	test_1.c
	gcc $(CFLAGS) test_1.c
