class Map:
  def __init__(self):
    self.Colors=[]

  def AddColor(self,color):
    self.Colors.append(color)

  def Save(self,filename):
    fh=open(filename,"w")
    for color in self.Colors:
      fh.write(f"{color.R} {color.G} {color.B}\n")
    fh.close()

  def ToPPM(self,filename):
    N=256
    HEIGHT=10
    fh=open(filename,"w")
    fh.write("P3\n")
    fh.write(f"# {filename}\n")
    fh.write(f"{N} {HEIGHT}\n")
    fh.write(f"{N-1}\n");
    for i in range(HEIGHT):
      for color in self.Colors:
        fh.write(f"{color.R} {color.G} {color.B}\n")
    fh.close()
