import json
import operator
from Color import Color
from Point import Point

class Gradient:
  def __init__(self,size):
    self.Name=""
    self.Size=size
    self.Points=[]
    self.Map=None
    #self.Colors=[]

  def __repr__(self):
    s="Size: "+str(self.Size)+"\n"
    for point in self.Points:
      s+="  "+str(point)+"\n"
    return(s)

  @staticmethod
  def Load(filename):
    gradient=None
    try:
      d=None
      fh=open(filename,"r")
      d=json.load(fh)
      size=256
      if "Size" in d:
        size=d["Size"]
      gradient=Gradient(size)
      if "Points" in d:
        l=d["Points"]
        #print(l)
        for dictp in l:
          gradient.AddPoint(Point.FromJsonDict(dictp))
      fh.close()
      gradient.SortPoints()
    except Exception as e:
      print("Load",e)
    return(gradient)

  def Save(self,filename):
    try:
      d={}
      d["Size"]=self.Size
      if self.Name!=None and self.Name!="":
        d["Name"]=self.Name
      if self.Points!=None:
        d["Points"]=[]
        for point in self.Points:
          d["Points"].append(point.GetDictForJson())
      print(d)
      fh=open(filename,"w")
      json.dump(d,fh)
      fh.close()
    except Exception as e:
      print("Save",e);

  def AddPoint(self,point):
    self.Points.append(point)

  """
  def Create(self,start,end):
    N=255
    interval_r=end.R-start.R;
    interval_g=end.G-start.G;
    interval_b=end.B-start.B;
    self.Colors.append(start)
    for i in range(1,N):
      self.Colors.append(Color(int(0.5+start.R+(i*interval_r)/N),int(0.5+start.G+(i*interval_g)/N),int(0.5+start.B+(i*interval_b)/N)))
    self.Colors.append(end)
  """

  def SortPoints(self):
    self.Points.sort(key=operator.attrgetter("Position"))
    pass

  def GenerateMap(self):
    try:
      self.Map=[]
      self.SortPoints()
      start=self.Points[0]
      self.Map.append(start.Color)
      for point in self.Points[1:]:
        n=point.Position-start.Position
        interval_r=point.Color.R-start.Color.R
        interval_g=point.Color.G-start.Color.G
        interval_b=point.Color.B-start.Color.B
        for i in range(1,n):
          self.Map.append(Color(int(0.5+start.Color.R+(i*interval_r)/n),int(0.5+start.Color.G+(i*interval_g)/n),int(0.5+start.Color.B+(i*interval_b)/n)))
        self.Map.append(point.Color)
        start=point
    except Exception as e:
      print("GenerateMap",e);

  def SaveMap(self,filename):
    fh=open(filename,"w")
    for color in self.Map:
      fh.write(f"{color.R} {color.G} {color.B}\n")
    fh.close()

  def MapToPPM(self,filename):
    N=256
    HEIGHT=10
    fh=open(filename,"w")
    fh.write("P3\n")
    fh.write(f"# {filename}\n");
    fh.write(f"{self.Size} {HEIGHT}\n");
    fh.write(f"{N-1}\n");
    for i in range(HEIGHT):
      for color in self.Map:
        fh.write(f"{color.R} {color.G} {color.B}\n")
    fh.close()
