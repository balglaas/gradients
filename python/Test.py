from Color import Color
from Gradient import Gradient

class Test:
  def __init__(self,start,end,basename):
    self.Start=start
    self.End=end
    self.Basename=basename

  def Run(self):
    gradient=Gradient()
    gradient.Create(self.Start,self.End)
    gradient.ToMap(self.Basename+".map")
    gradient.ToPPM(self.Basename+".ppm")
