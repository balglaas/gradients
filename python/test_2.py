from Gradient import Gradient
from Point import Point
from Color import Color

gradient=Gradient(256)
gradient.AddPoint(Point(100,Color(23,128,3)))
gradient.AddPoint(Point(200,Color(227,0,234)))
gradient.AddPoint(Point(255,Color(255,255,0)))
gradient.AddPoint(Point(0,Color(0,0,0)))
gradient.Save("/tmp/test.json")
print(gradient)
