from Color import Color

class Point:
  def __init__(self,position,color):
    self.Position=position
    self.Color=color

  def __repr__(self):
    #return(str(f"{self.Position}"))
    return(str(self.Position)+" "+str(self.Color))

  def GetDictForJson(self):
    d={}
    d["Position"]=self.Position
    d["Color"]=self.Color.GetDictForJson()
    return(d)  

  @staticmethod
  def FromJsonDict(d):
    #print("FromJsonDict",d)
    position=0
    color=Color(0,0,0)
    if "Position" in d:
      position=d["Position"]
    if "Color" in d:
      color=Color.FromJsonDict(d["Color"])
    return(Point(position,color))
