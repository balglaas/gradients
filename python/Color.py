class Color:
  def __init__(self,r,g,b):
    self.R=r
    self.G=g
    self.B=b

  def __repr__(self):
    #return(str(f"{self.Position}"))
    return("("+str(self.R)+","+str(self.G)+","+str(self.B)+")")
    
  def Output(self):
    print("%d %d %d\n" % (self.R,self.G,self.B))

  def GetDictForJson(self):
    d={}
    d["R"]=self.R
    d["G"]=self.G
    d["B"]=self.B
    return(d)

  @staticmethod
  def FromJsonDict(d):
    #print("Color",d)
    r=g=b=0  
    if "R" in d:
      r=d["R"]
    if "G" in d:
      g=d["G"]
    if "B" in d:
      b=d["B"]
    return(Color(r,g,b))
