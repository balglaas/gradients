from Gradient import Gradient
from Point import Point
from Color import Color

gradient=Gradient.Load("/tmp/test.json")
print(gradient)
gradient.GenerateMap()
gradient.SaveMap("/tmp/test.map")
gradient.MapToPPM("/tmp/test.ppm")
